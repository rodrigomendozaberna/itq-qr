export default function Footer() {
    return (
        <footer class="footer bg-white relative pt-1 border-b-2 border-grey-700">
            <div class="container mx-auto px-6">

                <div class="sm:flex sm:mt-8">
                    <div class="mt-8 sm:mt-0 sm:w-full sm:px-8 flex flex-col md:flex-row justify-between">
                        <div class="flex flex-col">
                            <span class="font-bold text-gray-700 uppercase mt-4 md:mt-0 mb-2">DOMICILIO:</span>
                            <span class="my-2"><a href="#" class="text-gray-700 text-md hover:text-blue-500">
                                Av.  Constituyentes Ote., No. 20, Centro
                            <br/>
                                C.P. 76000. Santiago de Querétaro, Querétaro.
                            </a>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container mx-auto px-6">
                <div class="mt-16 border-t-2 border-gray-700 flex flex-col items-center">
                    <div class="sm:w-2/3 text-center py-6">
                        <p class="text-sm text-grey-700 font-bold mb-2">
                            © {(new Date().getFullYear())} IQT
                        </p>
                    </div>
                </div>
            </div>
        </footer>
    )
}