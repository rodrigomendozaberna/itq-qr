export default function NavBar() {
    return (
        <nav class="bg-white-800 shadow-md">
            <div class="max-w-7xl mx-auto px-2 sm:px-6 lg:px-8">
                <div class="relative flex items-center justify-between h-16">
                    <div class="flex-1 flex items-center justify-center sm:items-stretch sm:justify-start">
                        <div class="flex-shrink-0 flex items-center">
                            <img class="block lg:hidden h-28 w-auto"
                                 src={require('../../assets/logo.png')}
                                 alt="Workflow"/>
                            <img class="hidden lg:block h-28 w-auto"
                                 src={require('../../assets/logo.png')}
                                 alt="Workflow"/>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    )
}