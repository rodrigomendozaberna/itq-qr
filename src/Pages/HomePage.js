import Layout from "../Components/Layout/Layout";
import {Link} from "react-router-dom";

export default function HomePage() {
    return (
        <Layout>
            <section class="text-gray-600 body-font">
                <div class="container px-5 py-10 mx-auto">
                    <div class="flex flex-wrap -m-4">

                        <div className="p-4 md:w-1/3">
                            <Link to="/quejas">
                                <div
                                    className="h-full rounded-xl shadow-cla-blue bg-gradient-to-r from-indigo-50 to-blue-50 overflow-hidden">
                                    <img
                                        className="lg:h-48 md:h-36 w-full object-cover object-center scale-110 transition-all duration-400 hover:scale-100"
                                        src="https://www.iqt.gob.mx/wp-content/uploads/2017/09/iqt-Logo-grande-alpha.png"
                                        alt="blog"/>
                                </div>
                            </Link>
                        </div>
                        <div class="p-4 md:w-1/3">
                            <Link to="tracking">
                                <div
                                    className="h-full rounded-xl shadow-cla-violate bg-gradient-to-r from-pink-50 to-red-50 overflow-hidden">
                                    <img
                                        className="lg:h-48 md:h-36 w-full object-cover object-center scale-110 transition-all duration-400 hover:scale-100"
                                        src="https://images.unsplash.com/photo-1624628639856-100bf817fd35?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8M2QlMjBpbWFnZXxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=600&q=60"
                                        alt="blog"/>
                                </div>
                            </Link>
                        </div>
                    </div>
                </div>
            </section>
        </Layout>
    );
}
